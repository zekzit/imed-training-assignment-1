package com.imed.tutorial.tracker.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.imed.tutorial.tracker.model.Project;
import com.imed.tutorial.tracker.model.Task;
import com.imed.tutorial.tracker.repository.TaskRepository;
import com.imed.tutorial.tracker.service.TaskService;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskRepository taskRepository;
	
	@Override
	public Optional<Task> findTaskById(Long id) {
		Optional<Task> task = taskRepository.findById(id);
		return task;
	}

	@Override
	public Task persistTask(Task task) {
	    Task savedTask = taskRepository.save(task);
		return savedTask;
	}

}
