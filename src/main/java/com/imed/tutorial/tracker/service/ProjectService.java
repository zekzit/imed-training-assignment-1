package com.imed.tutorial.tracker.service;

import java.util.List;
import java.util.Optional;

import com.imed.tutorial.tracker.model.Project;

public interface ProjectService {

	List<Project> listAllProjects();
	
	Project persistProject(Project project);

	Optional<Project> findProjectById(Long id);

}
