package com.imed.tutorial.tracker.service;

import java.util.Optional;

import com.imed.tutorial.tracker.model.Task;

public interface TaskService {

	Optional<Task> findTaskById(Long id);

	Task persistTask(Task task);

}
