package com.imed.tutorial.tracker.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.imed.tutorial.tracker.model.Project;
import com.imed.tutorial.tracker.repository.ProjectRepository;
import com.imed.tutorial.tracker.service.ProjectService;

@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectRepository projectRepository;
	
	@Override
	public List<Project> listAllProjects() {
		return (List<Project>) projectRepository.findAll();
	}

	@Override
	public Project persistProject(Project project) {
	    Project savedProject = projectRepository.save(project);
		return savedProject;
	}

	@Override
	public Optional<Project> findProjectById(Long id) {
		Optional<Project> project = projectRepository.findById(id);
		return project;
	}
	
}
