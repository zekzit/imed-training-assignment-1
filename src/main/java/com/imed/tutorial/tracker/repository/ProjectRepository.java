package com.imed.tutorial.tracker.repository;

import org.springframework.data.repository.CrudRepository;

import com.imed.tutorial.tracker.model.Project;

public interface ProjectRepository extends CrudRepository<Project, Long> {

}
