package com.imed.tutorial.tracker.repository;

import org.springframework.data.repository.CrudRepository;

import com.imed.tutorial.tracker.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}
