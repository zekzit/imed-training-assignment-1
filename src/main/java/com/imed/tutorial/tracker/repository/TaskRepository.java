package com.imed.tutorial.tracker.repository;

import org.springframework.data.repository.CrudRepository;

import com.imed.tutorial.tracker.model.Task;

public interface TaskRepository extends CrudRepository<Task, Long> {

}
