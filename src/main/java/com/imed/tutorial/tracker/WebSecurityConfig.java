package com.imed.tutorial.tracker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${imed.user.manager.username}")
	private String managerUsername;

	@Value("${imed.user.manager.password}")
	private String managerPassword;

	@Value("${imed.user.staff.username}")
	private String staffUsername;

	@Value("${imed.user.staff.password}")
	private String staffPassword;

	@Autowired
	private PasswordEncoder encoder;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
//		http.authorizeRequests().antMatchers("/login*").permitAll()

		http.authorizeRequests().antMatchers("/").permitAll().antMatchers("/h2-console/**").permitAll().anyRequest()
				.authenticated().and().httpBasic();

		http.csrf().disable();
		http.headers().frameOptions().disable();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser(managerUsername).password(encoder.encode(managerPassword))
				.roles("MANAGER");

		auth.inMemoryAuthentication().withUser(staffUsername).password(encoder.encode(staffPassword)).roles("STAFF");
	}
}
