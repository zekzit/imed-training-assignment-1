package com.imed.tutorial.tracker.web.dto;

import java.time.LocalDateTime;

import com.imed.tutorial.tracker.model.Employee;

public class TaskDescriptionDto {
	private Long id;
	private String name;
	private EmployeeDescriptionDto assignee;
	private String solutionDescription;
	private LocalDateTime startDateTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EmployeeDescriptionDto getAssignee() {
		return assignee;
	}

	public void setAssignee(EmployeeDescriptionDto assignee) {
		this.assignee = assignee;
	}

	public String getSolutionDescription() {
		return solutionDescription;
	}

	public void setSolutionDescription(String solutionDescription) {
		this.solutionDescription = solutionDescription;
	}

	public LocalDateTime getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(LocalDateTime startDateTime) {
		this.startDateTime = startDateTime;
	}

	public LocalDateTime getFinishDateTime() {
		return finishDateTime;
	}

	public void setFinishDateTime(LocalDateTime finishDateTime) {
		this.finishDateTime = finishDateTime;
	}

	private LocalDateTime finishDateTime;
}
