package com.imed.tutorial.tracker.web.dto;

import java.util.List;

public class ProjectDescriptionDto {
	private Long id;
	private String name;
	private EmployeeDescriptionDto projectManager;
	private float progress;
	private List<TaskDescriptionDto> tasks;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EmployeeDescriptionDto getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(EmployeeDescriptionDto projectManager) {
		this.projectManager = projectManager;
	}

	public List<TaskDescriptionDto> getTasks() {
		return tasks;
	}

	public void setTasks(List<TaskDescriptionDto> tasks) {
		this.tasks = tasks;
	}

	public float getProgress() {
		return progress;
	}

	public void setProgress(float progress) {
		this.progress = progress;
	}
}
