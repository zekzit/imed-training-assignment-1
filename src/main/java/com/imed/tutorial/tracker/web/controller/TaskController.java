package com.imed.tutorial.tracker.web.controller;

import java.time.LocalDateTime;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import com.imed.tutorial.tracker.model.Project;
import com.imed.tutorial.tracker.model.Task;
import com.imed.tutorial.tracker.service.ProjectService;
import com.imed.tutorial.tracker.service.TaskService;
import com.imed.tutorial.tracker.web.dto.TaskCreationDto;

@Controller
@RequestMapping("/tasks")
public class TaskController {

	@Autowired
	private ProjectService projectService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private DozerBeanMapper mapper;

	@GetMapping("/new")
	@Secured({ "ROLE_MANAGER" })
	public String newTask(Model model, @RequestParam(name = "project_id") Long projectId) {
		TaskCreationDto taskCreationDto = new TaskCreationDto();
		taskCreationDto.setProjectId(projectId);
		model.addAttribute("project", projectService.findProjectById(projectId)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
		model.addAttribute("taskCreationDto", taskCreationDto);
		return "tasks/form";
	}

	@GetMapping("/{id}/edit")
	public String editTask(@PathVariable Long id, Model model) {
		Task task = taskService.findTaskById(id).get();
		TaskCreationDto taskCreationDto = convertTaskToDto(task);
		model.addAttribute("project", task.getProject());
		model.addAttribute("taskCreationDto", taskCreationDto);
		return "tasks/form";
	}
	
	@GetMapping("/{id}")
	public String getTaskById(@PathVariable Long id, Model model) {
		Task task = taskService.findTaskById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		model.addAttribute("task", task);
		return "tasks/show";
	}
	
	@GetMapping("/{id}/start")
	public String startTask(@PathVariable Long id) {
		Task task = taskService.findTaskById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		task.setStartDateTime(LocalDateTime.now());
		taskService.persistTask(task);
		return "redirect:/tasks/" + id;
	}
	
	@GetMapping("/{id}/finish")
	public String finishTask(@PathVariable Long id) {
		Task task = taskService.findTaskById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		task.setFinishDateTime(LocalDateTime.now());
		taskService.persistTask(task);
		return "redirect:/tasks/" + id;
	}

	@PostMapping
	@Secured({ "ROLE_MANAGER" })
	public String createTask(TaskCreationDto taskDto) {
		Project project = projectService.findProjectById(taskDto.getProjectId()).get();
		Task task = convertTaskToEntity(taskDto);
		taskService.persistTask(task);
		project.getTasks().add(task);
		projectService.persistProject(project);
		return "redirect:/projects/" + taskDto.getProjectId();
	}

	protected TaskCreationDto convertTaskToDto(Task entity) {
		TaskCreationDto dto = new TaskCreationDto();
		mapper.map(entity, dto);
		return dto;
	}

	protected Task convertTaskToEntity(TaskCreationDto dto) {
		Task task = new Task();
		mapper.map(dto, task);
		return task;
	}
}
