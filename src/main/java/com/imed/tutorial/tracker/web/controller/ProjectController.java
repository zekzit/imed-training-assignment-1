package com.imed.tutorial.tracker.web.controller;

import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

import com.imed.tutorial.tracker.model.Project;
import com.imed.tutorial.tracker.service.ProjectService;
import com.imed.tutorial.tracker.web.dto.ProjectDescriptionDto;

@Controller
@RequestMapping("/projects")
public class ProjectController {

	@Autowired
	private ProjectService projectService;
	@Autowired
	private DozerBeanMapper mapper;

	@GetMapping
	public String listProjects(Model model) {
		List<Project> projects = projectService.listAllProjects();
		model.addAttribute("projects", projects);
		return "projects/index";
	}

	@GetMapping("/new")
	@Secured({ "ROLE_MANAGER" })
	public String newProject(Model model) {
		model.addAttribute("project", new Project());
		return "projects/form";
	}

	@GetMapping("/{id}/edit")
	@Secured({ "ROLE_MANAGER" })
	public String editProject(@PathVariable Long id, Model model) {
		Project project = projectService.findProjectById(id).get();
		model.addAttribute("project", project);
		return "projects/form";
	}

	@PostMapping
	@Secured({ "ROLE_MANAGER" })
	public String createProject(Project project) {
		projectService.persistProject(project);
		return "redirect:/projects";
	}

	@GetMapping("/{id}")
	public String getProjectById(@PathVariable Long id, Model model) {
		Project project = projectService.findProjectById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
//		ProjectDescriptionDto projectDescriptionDto = convertProjectToDto(project);
		model.addAttribute("project", project);
		return "projects/show";
	}
	
	protected ProjectDescriptionDto convertProjectToDto(Project entity) {
		ProjectDescriptionDto dto = new ProjectDescriptionDto();
		mapper.map(entity, dto);
		return dto;
	}

	protected Project convertProjectToEntity(ProjectDescriptionDto dto) {
		Project project = new Project();
		mapper.map(dto, project);
		return project;
	}
}
