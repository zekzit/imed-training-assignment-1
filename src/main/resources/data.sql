INSERT INTO employee (firstname, lastname, employee_type) VALUES ('John', 'Doe', 'MANAGER');
INSERT INTO employee (firstname, lastname, employee_type) VALUES ('Alice', 'Turtle', 'STAFF');

INSERT INTO project (name) VALUES ('Project A');
INSERT INTO project (name) VALUES ('Project B');

INSERT INTO task (name, project_id, assignee_id) VALUES ('Task A', 1, 2);
INSERT INTO task (name, project_id, assignee_id) VALUES ('Task B', 1, 2);

